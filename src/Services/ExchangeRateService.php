<?php

namespace App\Services;

/**
 *Exchange currency rates class
 *
 *This class to gets exchange currency rates from webservices
 *
 */
class ExchangeRateService
{
    private $sources = [];
    private $url;
    private $data;
    private $currentKey;
    private $values;
    private $rate;
    private $sourceName;
    private $priority;
    
    function __construct($sources) {
        $this->setDataSource($sources);
        $this->setData();
        $this->formatData();
        $this->setRate();
        $this->getExchangeRate();
        $this->getExchangeRateSourceName();
    }

    protected function setDataSource(array $sources) {
        $priority = [];
        foreach ($sources as $key => $value) {
            $value = ExchangeRateService::stdToArray($value);
            $sources[$key] = $value;
            $priority[$key] = $value["priority"];
        }
        array_multisort($priority, SORT_ASC, $sources);

        $this->sources = $sources;
    }

    protected function setData() {
        $sources = $this->sources;
        $i = 0;
        foreach ($sources as $value) {
            $url = $value["url"];
            if(ExchangeRateService::curl_get_file_contents($url) !== false) {
                $data = file_get_contents($url);
                $data = trim($data);
                $url = $value["url"];
                $currentKey = $value["params"];
                $sourceName = $value["name"];
                $priority = $value["priority"];
            }else{
                $i++;
                continue;
            }
            $this->url = $url;
            $this->data = $data;
            $this->currentKey = $currentKey;
            $this->sourceName = $sourceName;
            $this->priority = $priority;
            return true;
        } return false;
    }
    
    protected function formatData() {
        
        $data = $this->data;
        
        $values = json_decode($data, true);
        if($values) {
            $this->values = $values;
            return true;
        }
        
        $pars = xml_parser_create();
        xml_parse_into_struct($pars, $data, $values, $index);
        xml_parser_free($pars);
        if($values) {
            $this->values = $values;
            return true;
        }
    }

    public function setRate() {
        $values = $this->values;
        $currentKey = explode( "/", $this->currentKey );
        
        $tmp = $values;
        foreach($currentKey as $key) {
            $tmp =$tmp[$key];
        }
        $this->rate = $tmp;
    }
    
    public function getExchangeRate() {
        $rate = $this->rate;
        $rate = self::clean($rate);
        return $rate;
    }

    public function getExchangeRateSourceName() {
        $sourceName = $this->sourceName;
        return $sourceName;
    }

    function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
            else return FALSE;
    }
    
    function clean($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = strip_tags($data);
        return $data;
    }

    function stdToArray($obj){
      $array = (array)$obj;
      foreach($array as $key => &$field){
        if(is_object($field))$field = stdToArray($field);
      }
      return $array;
    }
}