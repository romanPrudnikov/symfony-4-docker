## QuickStart

1) git clone https://romanPrudnikov@bitbucket.org/romanPrudnikov/symfony-4-docker.git

2) cd symfony-4-docker/

3) docker-compose build

4) docker-compose up -d

5) docker exec -it -u dev sf4_php bash (or sudo docker exec -it -u dev sf4_php bash)

6) cd sf4/

7) composer install

8) open link in your browser: http://localhost/ru/blog/